# reaction

Welcome on reaction's Wiki!

For an introduction to reaction, see:
- Its [README](https://framagit.org/ppom/reaction)
- The tutorial, in [French](https://blog.ppom.me/fr-reaction) and in [English](https://blog.ppom.me/en-reaction).
- The example, fully commented configuration file, in [YAML](https://framagit.org/ppom/reaction/-/blob/main/app/example.yml) or in [JSONnet](https://framagit.org/ppom/reaction/-/blob/main/config/example.jsonnet).

This wiki is made of:

- [security.md](/security.md): good practices to avoid giving arbitrary execution to attackers. A must read!
- [jsonnet.md](/jsonnet.md): FAQ, help and good practices about JSONnet.
- [streams.md](/streams.md): good practices about stream sources
- [patterns.md](/patterns.md): good practices and common defaults about patterns
- [filters](/filters): discover existing service configurations
- [actions](/actions): discover existing actions
- [configurations](/configurations): discover real-world user configurations

❤️ Please enhance this wiki with your own discoveries! ❤️
