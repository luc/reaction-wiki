# ⚠️ Important security notice

Be careful when writing regexes. Try to ensure no malicious input could be injected in your regexes.
It's better if your actions are direct commands, and not inline shell scripts.

If you use products of regexes in shell scripts, double-check that all user input is correctly escaped.
Make use of tools like [shellcheck](https://www.shellcheck.net/) to analyze your code.

Avoid using this kind of commands that mix code and user input:
```jsonnet
['sh', '-c', 'mycommand <pattern>']
```

Example of a configuration permitting remote execution:

`insecure.jsonnet`
```jsonnet
{
  patterns: {
    user: {
      regex: @'.*',
    },
  },
  streams: {
    myservice: {
      cmd: ['tail', '-f', '/tmp/reaction-example'],
      filters: {
        myfilter: {
          regex: [
            @'Connection of <user> failed',
          ],
          // [...]
          actions: {
            myaction: {
              cmd: ['sh', '-c', 'echo "<user>"'],
            },
          },
        },
      },
    },
  },
}
```

Let's launch reaction in one terminal:
```shell
$ touch /tmp/reaction-example
$ reaction -c insecure.jsonnet
```

Then let's append malicious data to the `tail`ed file in another terminal:
```shell
$ echo 'Connection of "; mkdir malicious-directory" failed' >> /tmp/reaction-example
```

We simulated an attacker supplying `"; mkdir malicious-directory"` as a username in a random service
which doesn't check for non-alphanumeric characters in its usernames.

reaction will then launch the command:
```jsonnet
myaction: {
  cmd: ['sh', '-c', 'echo "<user>"'],
},
```

Substituting `<user>` with the malicious input:
```jsonnet
['sh', '-c', 'echo ""; mkdir malicious-directory""']
```

Of course, here it's a `mkdir` but it can be anything.

## Summary

Regexes are powerful, and need to be carefully written.
Avoid having too permissive regexes capturing user input.

When executing scripts, code and user input must be clearly separated.
Save scripts in files and call them like this:
`['/path/to/script', 'arg1', '...']` or `['bash', '/path/to/script', 'arg1', '...']`
instead of having inline
`['bash', '-c', 'command arg1 && command arg2']`
when dealing with user input.
